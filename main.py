from fastapi import FastAPI
from auth.routes import auth_router
from fastapi_jwt_auth import AuthJWT
from config import Settings
from products.routes import product_router
from cart.routes import cart_router
from order.routes import order_router

app = FastAPI()

@AuthJWT.load_config
def get_config():
    return Settings()


app.include_router(auth_router)
app.include_router(product_router)
app.include_router(cart_router)
app.include_router(order_router)

