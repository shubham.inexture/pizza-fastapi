from fastapi import APIRouter, status, Depends
from database import Session, engine
from fastapi_jwt_auth import AuthJWT
from fastapi.exceptions import HTTPException
from .schemas import OrderSchema, OrderStatusSchema
from cart.models import Cart
from auth.models import User
from .models import Order
from products.models import Product
from fastapi.encoders import jsonable_encoder
from .order_helper import OrderHelper
import datetime

order_router = APIRouter(
    prefix='/order',
    tags=['order']

)
session = Session(bind=engine)

@order_router.post('/order-product',
                     status_code=status.HTTP_201_CREATED
                     )
async def order_product(cart: OrderSchema , Authorize:AuthJWT=Depends()):
    """
     Api to add order instructions and place order
    """
    try:
        Authorize.jwt_required()

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Token") from e
    username=Authorize.get_jwt_subject()
    user = session.query(User).filter(User.username == username).first()
    product_total = 0
    product_list = session.query(Cart).filter(Cart.user_id==user.id).all()
    cart_list = []
    cart_data = {'data':cart_list}
    for product in product_list:
        product_object = session.query(Product).filter(product.product_id == Product.id).first()
        if product.quantity>product_object.stock_available:
            raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail=f"Given quantity is not available for the product {product.product_name}")

    for product in product_list:
        product_object = session.query(Product).filter(product.product_id == Product.id).first()
        product_price = product.quantity*product_object.price
        product_total+=product_price
        product_object.stock_available-=product.quantity
        product_data = {'quantity': product.quantity, 'product_name': product_object.product_name, 'product_id': product_object.id}

        cart_list.append(product_data)
        session.delete(product)


    new_order = Order(
        mobile_number = cart.mobile_number,
        address = cart.address,
        receiver_name = cart.receiver_name,
        product_total = product_total,
        order_status = 'ORDER_RECEIVED',
        user_id = user.id,
        product_details = cart_data
    )

    session.add(new_order)
    session.commit()

    response = {

            "mobile_number": cart.mobile_number,
            "address": cart.address,
            "receiver_name": cart.receiver_name,
            "product_total": product_total,
            "order_status" : "ORDER_RECEIVED"


    }

    return jsonable_encoder(response)


@order_router.get('/get-order-detail-by-id/{id}',
                     status_code=status.HTTP_200_OK
                     )
async def get_product_details(id: int, Authorize:AuthJWT=Depends()):
    """
     Api to get order details of a particular user
    """
    try:
        Authorize.jwt_required()

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Token") from e

    username=Authorize.get_jwt_subject()
    user = session.query(User).filter(User.username == username).first()
    order_detail = session.query(Order).filter(Order.id==id).first()
    if order_detail.user_id==user.id:
        response = OrderHelper.object_as_dict(order_detail)
        return jsonable_encoder(response)

    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized")


@order_router.get('/get-order-detail-by-id/{id}',
                     status_code=status.HTTP_200_OK
                     )
async def get_order_details(id: int, Authorize:AuthJWT=Depends()):
    """
     Api to get order details of a particular user by id
    """
    try:
        Authorize.jwt_required()

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Token") from e

    username=Authorize.get_jwt_subject()
    user = session.query(User).filter(User.username == username).first()
    order_detail = session.query(Order).filter(Order.id==id).first()
    if order_detail.user_id==user.id:
        response = OrderHelper.object_as_dict(order_detail)
        return jsonable_encoder(response)

    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized")

@order_router.patch('/update-order-status/{id}',
                     status_code=status.HTTP_200_OK
                     )
async def update_order_status(id: int, order: OrderStatusSchema, Authorize:AuthJWT=Depends()):
    """
     Api to update order details of a particular user by a staff
    """
    try:
        Authorize.jwt_required()

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Token") from e

    username=Authorize.get_jwt_subject()
    user = session.query(User).filter(User.username == username).first()
    if not user.is_staff:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User is not a staff user"
        )
    order_detail = session.query(Order).filter(Order.id==id).first()
    order_detail.order_status = order.order_status
    session.commit()
    response = {
        "order_id": order_detail.id,
        "order_status": order_detail.order_status
    }

    return jsonable_encoder(response)


@order_router.patch('/cancel-order/{id}',
                     status_code=status.HTTP_200_OK
                     )
async def cancel_order_by_user(id: int, order: OrderStatusSchema, Authorize:AuthJWT=Depends()):
    """
     Api to cancel order for a user
    """
    try:
        Authorize.jwt_required()

    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Token") from e

    username=Authorize.get_jwt_subject()
    user = session.query(User).filter(User.username == username).first()
    order_detail = session.query(Order).filter(Order.id==id).first()
    current_time = datetime.datetime.now()
    time_diff = current_time - order_detail.creation_date
    if order_detail.user_id==user.id and order.order_status=='ORDER_CANCELLED' and time_diff.seconds<=120:
        order_detail.order_status = order.order_status
        session.commit()
        response = {
            "order_id": order_detail.id,
            "order_status": order_detail.order_status
        }
        return jsonable_encoder(response)

    if order_detail.user_id!=user.id:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User is not authorized to perform action for this order"
        )

    if order.order_status!='ORDER_CANCELLED':
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User is not authorized to perform this action"
        )

    if time_diff.seconds>120:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User can't cancel the order after it exceeds the time limit of 2 minutes"
        )

