from sqlalchemy import inspect
class OrderHelper:
    """Helper function which converts a model object into json format"""
    @staticmethod
    def object_as_dict(obj):
        return {c.key: getattr(obj, c.key)
                for c in inspect(obj).mapper.column_attrs}