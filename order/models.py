from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey,DateTime
from sqlalchemy_utils.types import ChoiceType
from sqlalchemy.dialects.postgresql import JSONB
import datetime


ORDER_STATUS = (
    ('ORDER_RECEIVED', 'order_received'),
    ('OUT_FOR_DELIVERY', 'out_for_delivery'),
    ('PENDING', 'pending'),
    ('ORDER_DELIVERED', 'order_delivered'),
    ('ORDER_CANCELLED', 'order_cancelled')
)

class Order(Base):
    """Order model which stores the order related information"""
    __tablename__ = 'order'
    id = Column(Integer, primary_key=True)
    mobile_number = Column(String, nullable = False)
    address = Column(String, nullable = False)
    receiver_name = Column(String, nullable = False)
    product_total = Column(Integer,nullable = False)
    order_status = Column(ChoiceType(choices=ORDER_STATUS), default = 'ORDER_RECEIVED')
    user_id = Column(Integer, ForeignKey("user.id"))
    product_details = Column(JSONB)
    creation_date = Column(DateTime, default=datetime.datetime.now)
